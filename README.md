### Pack planner programming challenge ###

* Version: 1.0
* Programming language: Java

### Summary ###

* TestPackPlanner is a console application which takes as standard input the sorting method, the max items per pack, the max pack weight, the list of items and sorts provided items into packs.
* The input ends when an empty line is received

#### Input format ####

[Sort order],[max pieces per pack],[max weight per pack]  
[item id],[item length],[item quantity],[piece weight]  
[item id],[item length],[item quantity],[piece weight]  
[item id],[item length],[item quantity],[piece weight]  

#### Output format ####

Pack Number: [pack number]  
[item id],[item length],[item quantity],[piece weight]  
[item id],[item length],[item quantity],[piece weight]  
  
Pack Length: [pack length], Pack Weight: [pack weight]  

#### Example input ####

NATURAL,40,500.0  
1001,6200,30,9.653  
2001,7200,50,11.21  

#### Example output ####

Pack Number: 1  
1001,6200,30,9.653  
2001,7200,10,11.21  
Pack Length: 7200, Pack Weight: 401.69  

Pack Number: 2  
2001,7200,40,11.21  
Pack Length: 7200, Pack Weight: 448.4  

### Unit tests ###

* The program has got unit tests prepared. To run the tests - open the project in Intellij IDEA and run tests on the folder "\src\test\java\com\test\packplanner"