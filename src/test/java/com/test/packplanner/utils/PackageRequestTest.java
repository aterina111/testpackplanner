package com.test.packplanner.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PackageRequestTest
{
    @Test
    @DisplayName("Create from input check")
    void createFromInputCheck() throws Exception
    {
        List<String> input = new ArrayList<>();
        input.add("NATURAL,90,568.2");
        input.add("1001,8500,10,9.653");
        PackageRequest packageRequest = PackageRequest.createFromInput(input);
        Assertions.assertAll(
                ()->assertEquals(Sorting.NATURAL, packageRequest.getSortingOrder()),
                ()->assertEquals(90, packageRequest.getMaxNumberOfPiecesPerPack()),
                ()->assertEquals(568.2, packageRequest.getMaxWeightPerPack()),
                ()->assertTrue(packageRequest.isValid())
        );
    }
}
