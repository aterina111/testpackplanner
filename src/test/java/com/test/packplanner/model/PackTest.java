package com.test.packplanner.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PackTest
{
    @Test
    @DisplayName("Check pack parameters")
    void checkPackParameters() {
        Pack pack = new Pack(1, 50, 563.8);
        pack.addItem(Item.createFromInput("1001,7400,20,7.8"));
        pack.addItem(Item.createFromInput("2001,6800,10,4.3"));
        pack.addItem(Item.createFromInput("3001,8500,30,8.5"));
        Assertions.assertAll(
                ()->assertEquals(8500, pack.getPackLength()),
                ()->assertEquals(454.0, pack.getPackWeight())
        );
    }

    @Test
    @DisplayName("Check consume")
    void checkConsume() {
        List<Item> itemToConsume = new ArrayList<>();
        itemToConsume.add(Item.createFromInput("1001,7400,20,11.8"));
        itemToConsume.add(Item.createFromInput("2001,6800,10,9.3"));
        itemToConsume.add(Item.createFromInput("3001,8500,30,16.5"));

        Pack pack = new Pack(1, 50, 563.8);
        itemToConsume = pack.consume(itemToConsume);
        int size = itemToConsume.size();
        int remainingItemId = itemToConsume.get(0).getItemId();
        int remainingQuantity = itemToConsume.get(0).getQuantity();


        Assertions.assertAll(
                ()->assertEquals(1, size),
                ()->assertEquals(560.0, pack.getPackWeight()),
                ()->assertEquals(3001, remainingItemId),
                ()->assertEquals(16, remainingQuantity)
        );
    }
}
