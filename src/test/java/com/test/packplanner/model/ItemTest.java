package com.test.packplanner.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ItemTest
{
    @Test
    @DisplayName("Create from input check")
    void createFromInputCheck() {
        Item item = Item.createFromInput("1001,8500,10,9.653");
        Assertions.assertAll(
                ()->assertEquals(1001, item.getItemId()),
                ()->assertEquals(8500, item.getLength()),
                ()->assertEquals(10, item.getQuantity()),
                ()->assertEquals(9.653, item.getWeight()),
                ()->assertEquals(96.53, item.getTotalWeight()),
                ()->assertTrue(item.isValid())
        );
    }

    @Test
    @DisplayName("Create from input check negative values")
    void createFromInputCheckNegativeValues() {
        Assertions.assertAll(
                ()->assertFalse(Item.createFromInput("-1001,8500,10,9.653").isValid()),
                ()->assertFalse(Item.createFromInput("1001,-8500,10,9.653").isValid()),
                ()->assertFalse(Item.createFromInput("1001,8500,-10,9.653").isValid()),
                ()->assertFalse(Item.createFromInput("1001,8500,10,-9.653").isValid())
        );
    }

    @Test
    @DisplayName("Create from input: check incorrect item id value")
    void createFromInputCheckIncorrectItemIdValue() {
        NumberFormatException exception = assertThrows(NumberFormatException.class, () -> {
            Item.createFromInput("ABC,8500,10,9.653");
        });

        assertTrue(exception.getMessage().contains("For input string: \"ABC\""));
    }

    @Test
    @DisplayName("Create from input: check incorrect length value")
    void createFromInputCheckIncorrectLengthValue() {
        NumberFormatException exception = assertThrows(NumberFormatException.class, () -> {
            Item.createFromInput("1001,8500.6,10,9.653");
        });

        assertTrue(exception.getMessage().contains("For input string: \"8500.6\""));
    }

    @Test
    @DisplayName("Create from input: check incorrect quantity value")
    void createFromInputCheckIncorrectQuantityValue() {
        NumberFormatException exception = assertThrows(NumberFormatException.class, () -> {
            Item.createFromInput("1001,8500,10.8,9.653");
        });

        assertTrue(exception.getMessage().contains("For input string: \"10.8\""));
    }

    @Test
    @DisplayName("Create from input: check incorrect weight value")
    void createFromInputCheckIncorrectWeightValue() {
        NumberFormatException exception = assertThrows(NumberFormatException.class, () -> {
            Item.createFromInput("1001,8500,10,9.653A");
        });

        assertTrue(exception.getMessage().contains("For input string: \"9.653A\""));
    }

    @Test
    @DisplayName("Create from input: check empty")
    void createFromInputCheckEmpty() {
        Exception exception = assertThrows(Exception.class, () -> {
            Item.createFromInput("");
        });

        String expectedMessage = "Not enough parameters to create an item from string.";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    @DisplayName("Check set packed quantity")
    void checkGetWeight() {
        Item item = Item.createFromInput("1001,8500,10,9.653");
        item.setPackedQuantity(8);
        assertEquals(2, item.getRemainingQuantity());
    }
}
