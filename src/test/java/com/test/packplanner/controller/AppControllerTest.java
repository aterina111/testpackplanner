package com.test.packplanner.controller;

import com.test.packplanner.model.Pack;
import com.test.packplanner.utils.PackageRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AppControllerTest
{
    @Test
    @DisplayName("Check empty input")
    void checkEmptyInput ()
    {
        Exception exception = assertThrows(Exception.class, () -> {
            AppController.createPacks(PackageRequest.createFromInput(new ArrayList<>()));
        });

        String expectedMessage = "No input data to configure package.";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    @DisplayName("Should check packs for items sorted natural")
    void shouldCheckPacksForItemsSortedNatural () throws Exception
    {
        List<String> input = getTestInput("NATURAL,40,500.0");
        List<Pack> packs = AppController.createPacks(PackageRequest.createFromInput(input));

        Assertions.assertLinesMatch(getExpectedOutputNatural(), getActualOutput(packs));
    }

    @Test
    @DisplayName("Should check packs for items sorted short to long")
    void shouldCheckPacksForItemsSortedShortToLong () throws Exception
    {
        List<String> input = getTestInput("SHORT_TO_LONG,40,300.0");
        List<Pack> packs = AppController.createPacks(PackageRequest.createFromInput(input));

        Assertions.assertLinesMatch(getExpectedOutputShortToLong(), getActualOutput(packs));
    }

    @Test
    @DisplayName("Should check packs for items sorted long to short")
    void shouldCheckPacksForItemsSortedLongToShort () throws Exception
    {
        List<String> input = getTestInput("LONG_TO_SHORT,50,500.0");
        List<Pack> packs = AppController.createPacks(PackageRequest.createFromInput(input));

        Assertions.assertLinesMatch(getExpectedOutputLongToShort(), getActualOutput(packs));
    }

    private List<String> getActualOutput(List<Pack> packs)
    {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < packs.size(); i++) {
            List<String> packOutput = packs.get(i).getOutput();
            for (int l = 0; l < packOutput.size(); l++)
                result.add(packOutput.get(l));
        }

        return result;
    }

    private List<String> getTestInput (String settings)
    {
        List<String> result = new ArrayList<>();
        result.add(settings);
        result.add("1001,8500,10,9.653");
        result.add("2001,6300,50,11.21");
        result.add("3001,7600,30,10.59");

        return result;
    }

    private List<String> getExpectedOutputNatural ()
    {
        List<String> expectedLines = new ArrayList<>();
        expectedLines.add("Pack number: 1");
        expectedLines.add("1001,8500,10,9.653");
        expectedLines.add("2001,6300,30,11.21");
        expectedLines.add("Pack length: 8500, Pack weight: 432.83");

        expectedLines.add("Pack number: 2");
        expectedLines.add("2001,6300,20,11.21");
        expectedLines.add("3001,7600,20,10.59");
        expectedLines.add("Pack length: 7600, Pack weight: 436.0");

        expectedLines.add("Pack number: 3");
        expectedLines.add("3001,7600,10,10.59");
        expectedLines.add("Pack length: 7600, Pack weight: 105.9");

        return expectedLines;
    }

    private List<String> getExpectedOutputShortToLong ()
    {
        List<String> expectedLines = new ArrayList<>();
        expectedLines.add("Pack number: 1");
        expectedLines.add("2001,6300,26,11.21");
        expectedLines.add("Pack length: 6300, Pack weight: 291.46");

        expectedLines.add("Pack number: 2");
        expectedLines.add("2001,6300,24,11.21");
        expectedLines.add("3001,7600,2,10.59");
        expectedLines.add("1001,8500,1,9.653");
        expectedLines.add("Pack length: 8500, Pack weight: 299.87");

        expectedLines.add("Pack number: 3");
        expectedLines.add("3001,7600,28,10.59");
        expectedLines.add("Pack length: 7600, Pack weight: 296.52");

        expectedLines.add("Pack number: 4");
        expectedLines.add("1001,8500,9,9.653");
        expectedLines.add("Pack length: 8500, Pack weight: 86.88");

        return expectedLines;
    }

    private List<String> getExpectedOutputLongToShort ()
    {
        List<String> expectedLines = new ArrayList<>();
        expectedLines.add("Pack number: 1");
        expectedLines.add("1001,8500,10,9.653");
        expectedLines.add("3001,7600,30,10.59");
        expectedLines.add("2001,6300,7,11.21");
        expectedLines.add("Pack length: 8500, Pack weight: 492.7");

        expectedLines.add("Pack number: 2");
        expectedLines.add("2001,6300,43,11.21");
        expectedLines.add("Pack length: 6300, Pack weight: 482.03");

        return expectedLines;
    }
}
