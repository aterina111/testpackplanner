package com.test.packplanner.controller;

import com.test.packplanner.model.Pack;
import com.test.packplanner.utils.PackageBuilder;
import com.test.packplanner.utils.PackageRequest;

import java.util.List;

public class AppController {
    public static List<Pack> createPacks(PackageRequest packageRequest) throws Exception
    {
        if(!packageRequest.isValid())
            throw new Exception("Package request data is invalid.");

        PackageBuilder packageBuilder = PackageBuilder.configure(packageRequest);
        packageBuilder.sortItems();
        packageBuilder.buildPacks();

        return packageBuilder.getPacks();
    }
}
