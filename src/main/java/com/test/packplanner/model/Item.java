package com.test.packplanner.model;

import com.test.packplanner.utils.Constants;

import static java.lang.Integer.parseInt;

public class Item implements Comparable<Item>
{
    private int itemId;
    private int quantity;
    private int length;
    private double weight;

    private int packedQuantity = 0;

    public Item (int itemId, int length, double weight, int quantity)
    {
        this.itemId = itemId;
        this.length = length;
        this.weight = weight;
        this.quantity = quantity;
    }

    public Item (Item item, int quantity)
    {
        this.itemId = item.getItemId();
        this.length = item.getLength();
        this.weight = item.getWeight();
        this.quantity = quantity;
    }

    public int getItemId()
    {
        return itemId;
    }
    public int getLength()
    {
        return length;
    }
    public double getWeight()
    {
        return weight;
    }
    public int getQuantity()
    {
        return quantity;
    }

    public double getTotalWeight()
    {
        return getWeight() * getQuantity();
    }

    public int getRemainingQuantity ()
    {
        return this.getQuantity() - this.packedQuantity;
    }

    public void setPackedQuantity(int packedQuantity)
    {
        this.packedQuantity += packedQuantity;
    }

    public boolean isValid()
    {
        return getItemId() > 0 && getLength() > 0 && getWeight() > 0 && getQuantity() > 0;
    }

    // Create object of class Item from string input
    public static Item createFromInput(String input)
    {
        String[] values = input.split(",");

        if (values.length < Constants.REQUIRED_ITEM_PARAMETERS)
            throw new IndexOutOfBoundsException("Not enough parameters to create an item from string.");

        try {
            int itemId = parseInt(values[Constants.ITEM_ID_INDEX]);
            int length = parseInt(values[Constants.ITEM_LENGTH_INDEX]);
            int quantity = parseInt(values[Constants.ITEM_QUANTITY_INDEX]);
            double weight = Double.parseDouble(values[Constants.ITEM_WEIGHT_INDEX]);

            return new Item(itemId, length, weight, quantity);
        }  catch (Exception e) {
            throw new NumberFormatException(e.toString());
        }
    }

    public String getOutput ()
    {
        return new StringBuilder()
                .append(itemId).append(",")
                .append(length).append(",")
                .append(quantity).append(",")
                .append(weight).toString();
    }

    @Override
    public int compareTo(Item other) {
        if(length == other.length)
            return 0;
        else if(length > other.length)
            return 1;
        else
            return -1;
    }
}
