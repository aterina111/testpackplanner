package com.test.packplanner.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Pack
{
    private int packId;
    private List<Item> items = new ArrayList<>();

    private int remainingQuantity;
    private double remainingWeight;

    public Pack (int packId, int maxNumberOfPieces, double maxWeight)
    {
        this.packId = packId;
        this.remainingQuantity = maxNumberOfPieces;
        this.remainingWeight = maxWeight;
    }

    public void addItem(Item item)
    {
        this.items.add(item);
    }

    public int getPackLength()
    {
        Collections.sort(items, Collections.reverseOrder());
        return items.get(0).getLength();
    }

    public double getPackWeight()
    {
        double result = 0;

        int size = items.size();
        for (int i = 0; i < size; i++)
            result += items.get(i).getTotalWeight();

        if(result > 0)
            result = (double) Math.round(result * 100) / 100;

        return result;
    }

    // Return result of program for printing and testing
    public List<String> getOutput ()
    {
        List<String> result = new ArrayList<>();
        result.add("Pack number: " + packId);

        for (int i = 0; i < items.size(); i++)
            result.add(items.get(i).getOutput());

        result.add(new StringBuilder()
                .append("Pack length: ").append(getPackLength()).append(", ")
                .append("Pack weight: ").append(getPackWeight()).toString());

        return result;
    }

    public void print() {
        List<String> output = getOutput();

        for (int i = 0; i < output.size(); i++)
            System.out.println(output.get(i));

        System.out.println();
    }

    // Return an updated list of items after applying allowed quantity for this particular pack
    public List<Item> consume (List<Item> itemsToConsume) {
        int size = itemsToConsume.size();

        for (int i = 0; i < size; i++) {
            int allowedQty = getAllowedQuantity(itemsToConsume.get(i));

            if(allowedQty > 0) {
                addItem(new Item(itemsToConsume.get(i), allowedQty));
                itemsToConsume.get(i).setPackedQuantity(allowedQty);
                this.remainingQuantity -= allowedQty;
                this.remainingWeight -= allowedQty * itemsToConsume.get(i).getWeight();
            }

            if (this.remainingQuantity == 0)
                break;
        }

        return getUpdatedList(itemsToConsume);
    }

    // Create a list of unpacked items
    private List<Item> getUpdatedList(List<Item> itemsToConsume)
    {
        List<Item> result = new ArrayList<>();

        int size = itemsToConsume.size();
        for (int i = 0; i < size; i++) {
            if(itemsToConsume.get(i).getRemainingQuantity() > 0)
                result.add(new Item(itemsToConsume.get(i), itemsToConsume.get(i).getRemainingQuantity()));
        }

        return result;
    }

    // Return allowed item quantity for the pack
    private int getAllowedQuantity(Item item)
    {
        int itemQuantity = item.getQuantity();
        double itemPieceWeight = item.getWeight();

        int qty = (int) (remainingWeight / itemPieceWeight);

        if (itemQuantity < qty)
            qty = itemQuantity;

        if (remainingQuantity < qty)
            qty = remainingQuantity;

        return qty;
    }
}
