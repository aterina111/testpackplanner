package com.test.packplanner.utils;

import com.test.packplanner.model.Item;
import com.test.packplanner.model.Pack;

import java.util.ArrayList;
import java.util.List;

public class PackageBuilder
{
    protected PackageRequest packageRequest;
    private List<Pack> packs = new ArrayList<>();

    public PackageBuilder (PackageRequest packageRequest)
    {
        this.packageRequest = packageRequest;
    }

    public static PackageBuilder configure (PackageRequest request) throws Exception
    {
        switch (request.getSortingOrder())
        {
            case Sorting.NATURAL:
                return new PackageBuilder(request);
            case Sorting.SHORT_TO_LONG:
                return new PackageBuilderShortToLong(request);
            case Sorting.LONG_TO_SHORT:
                return new PackageBuilderLongToShort(request);
            default:
                throw new Exception("Package building failed. No sorting order provided.");
        }
    }

    public List<Pack> getPacks()
    {
        return packs;
    }

    public void sortItems() {}

    public void buildPacks() throws Exception
    {
        if (packageRequest.getItems().size() == 0)
            throw new Exception("Nothing to pack...");

        List<Item> itemsToPack = packageRequest.getItems();

        int packageId = 0;
        while (itemsToPack.size() > 0) {
            ++ packageId;
            Pack pack = new Pack(packageId, packageRequest.getMaxNumberOfPiecesPerPack(), packageRequest.getMaxWeightPerPack());
            itemsToPack = pack.consume(itemsToPack);
            packs.add(pack);
        }
    }
}


