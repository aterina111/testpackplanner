package com.test.packplanner.utils;

import java.util.Collections;

public class PackageBuilderLongToShort extends PackageBuilder
{
    public PackageBuilderLongToShort(PackageRequest packageRequest) {
        super(packageRequest);
    }

    public void sortItems()
    {
        Collections.sort(packageRequest.getItems(), Collections.reverseOrder());
    }
}
