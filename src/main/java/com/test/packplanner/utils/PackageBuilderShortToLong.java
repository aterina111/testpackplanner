package com.test.packplanner.utils;

import java.util.Collections;

public class PackageBuilderShortToLong extends PackageBuilder
{
    public PackageBuilderShortToLong(PackageRequest packageRequest) {
        super(packageRequest);
    }

    public void sortItems()
    {
        Collections.sort(packageRequest.getItems());
    }
}
