package com.test.packplanner.utils;

public class Sorting {
    public static final int UNDEFINED = -1;
    public static final int NATURAL = 0;
    public static final int SHORT_TO_LONG = 1;
    public static final int LONG_TO_SHORT = 2;

    public static int getValue(String input)
    {
        switch (input) {
            case "NATURAL":
                return NATURAL;
            case "SHORT_TO_LONG":
                return SHORT_TO_LONG;
            case "LONG_TO_SHORT":
                return LONG_TO_SHORT;
            default:
                return UNDEFINED;
        }
    }
}
