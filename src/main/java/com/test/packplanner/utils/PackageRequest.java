package com.test.packplanner.utils;

import com.test.packplanner.model.Item;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.parseInt;

public class PackageRequest
{
    private int sortingOrder;
    private int maxNumberOfPiecesPerPack;
    private double maxWeightPerPack;
    private List<Item> items = new ArrayList<>();

    public PackageRequest(int sortingOrder, int maxNumberOfPiecesPerPack, double maxWeightPerPack)
    {
        this.sortingOrder = sortingOrder;
        this.maxNumberOfPiecesPerPack = maxNumberOfPiecesPerPack;
        this.maxWeightPerPack = maxWeightPerPack;
    }

    public int getSortingOrder()
    {
        return sortingOrder;
    }

    public int getMaxNumberOfPiecesPerPack()
    {
        return maxNumberOfPiecesPerPack;
    }

    public double getMaxWeightPerPack()
    {
        return maxWeightPerPack;
    }

    public List<Item> getItems()
    {
        return items;
    }

    // Create object of class PackageRequest from string input
    public static PackageRequest createFromInput(List<String> input) throws Exception
    {
        if(input.size() == 0)
            throw new IllegalArgumentException("No input data to configure package.");
        String[] settings = input.get(0).split(",");

        if(settings.length < Constants.REQUIRED_PACK_REQUEST_PARAMETERS)
            throw new IndexOutOfBoundsException("Not enough parameters to create a package from string.");

        try {
            int sorting = Sorting.getValue(settings[Constants.SORTING_METHOD_INDEX]);
            int maxPiecesPerPack = parseInt(settings[Constants.MAX_NUMBER_PER_PACK_INDEX]);
            double maxWeight = Double.parseDouble(settings[Constants.MAX_WEIGHT_PER_PACK_INDEX]);

            PackageRequest packageRequest = new PackageRequest(sorting, maxPiecesPerPack, maxWeight);

            for (int i = 1; i < input.size(); i++)
                packageRequest.addItem(Item.createFromInput(input.get(i)));

            return packageRequest;
        }  catch (Exception e) {
            throw new NumberFormatException(e.toString());
        }
    }

    public boolean isValid () throws Exception
    {
        if (getSortingOrder() < 0)
            throw new Exception("Package request data error. Sorting order is invalid.");
        if (getMaxNumberOfPiecesPerPack() <= 0)
            throw new Exception("Package request data error. Max number of pieces per package is invalid.");
        if (getMaxWeightPerPack() <= 0)
            throw new Exception("Package request data error. Max weight per package is invalid.");
        if (items.size() == 0)
            throw new Exception("Package request data error. Items data is not valid.");
        if (!isItemsValid())
            throw new Exception("Package request data error. Items data is not valid.");

        return true;
    }

    private void addItem(Item item)
    {
        this.items.add(item);
    }

    private boolean isItemsValid()
    {
        for (int i = 0; i < items.size(); i++)
            if(!items.get(i).isValid())
                return false;

        return true;
    }
}
