package com.test.packplanner.utils;

public class Constants {
    public static final int REQUIRED_PACK_REQUEST_PARAMETERS = 3;
    public static final int REQUIRED_ITEM_PARAMETERS = 4;

    // Pack input indexes
    public static final int SORTING_METHOD_INDEX = 0;
    public static final int MAX_NUMBER_PER_PACK_INDEX = 1;
    public static final int MAX_WEIGHT_PER_PACK_INDEX = 2;

    // Item input indexes
    public static final int ITEM_ID_INDEX = 0;
    public static final int ITEM_LENGTH_INDEX = 1;
    public static final int ITEM_QUANTITY_INDEX = 2;
    public static final int ITEM_WEIGHT_INDEX = 3;
}
