package com.test.packplanner;

import com.test.packplanner.controller.AppController;
import com.test.packplanner.model.Pack;
import com.test.packplanner.utils.PackageRequest;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main (String[] args) {
        System.out.println("Pack planner input:");
        try {
            List<String> input = getInput();
            List<Pack> packs = AppController.createPacks(PackageRequest.createFromInput(input));
            printOutput(packs);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private static List<String> getInput() throws Exception
    {
        List<String> result = new ArrayList<>();

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s;

        while ((s = br.readLine()) != null && s.length() != 0)
            result.add(s);

        return result;
    }

    private static void printOutput (List<Pack> packs)
    {
        for (int i = 0; i < packs.size(); i++)
            packs.get(i).print();
    }
}
